# ljnip

#### 介绍
1.  附近信息发布系统（零久附近通）
2.  开发公司：娄底零久网络科技有限公司。
3.  您可以使用我们的源代码去生成各种APP及小程序，我们提供稳定可靠的服务器端。
#### 示例
1.  微信小程序：请用手机微信APP扫一扫  
![微信小程序](https://gitee.com/h144111/ljnip/raw/master/weixin.png "微信小程序")
2.  [H5端：https://09love.44api.com](https://09love.44api.com)
3.  QQ小程序:请用手机QQ扫一扫  
![QQ小程序](https://gitee.com/h144111/ljnip/raw/master/qq.png "QQ小程序")
4.  百度小程序：请用手机百度APP扫一扫  
![百度小程序](https://gitee.com/h144111/ljnip/raw/master/baidu.png "百度小程序")
5.  [安卓APK](https://09love.44api.com/44api.apk)
6.  [苹果ipa](https://09love.44api.com/44api.ipa)
#### 软件架构
程序使用HBuilder X开发 项目类型为:uni-app


#### 安装教程
1.  请打开HBuilder X 点击文件->新建项目->选择uni-app->创建  
![第一步](https://gitee.com/h144111/ljnip/raw/master/step1.png "第一步")
2.  打开创建目录，将项目目录下的所有文件删除，把本源代码复制到项目目录中  
3.  打开manifest.json，重新获取APPID  
![第二步](https://gitee.com/h144111/ljnip/raw/master/step2.png "第二步")
3.  点击运行->运行到浏览器->chorme  
![第三步](https://gitee.com/h144111/ljnip/raw/master/step3.png "第三步")
3.  项目打开浏览器后点击F12  在调试窗口上方选择手机模式  
![第四步](https://gitee.com/h144111/ljnip/raw/master/step4.png "第四步")

#### 使用说明
1.  请重新配置manifest.json里面的参数
2.  可以商用，我们的服务器端长期稳定可靠
3.  栏目添加申请：[https://www.44api.com/f/add](https://www.44api.com/f/add)
4.  国际化，多语言翻译，请修改文件main.js
5.  国际化，多语言自动适配，请修改文件APP.vue
6.  字体图标采用[http://www.fontawesome.com.cn/](http://www.fontawesome.com.cn/)